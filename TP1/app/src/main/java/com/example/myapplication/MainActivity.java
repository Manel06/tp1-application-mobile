package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private static final String[] values = AnimalList.getNameArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*final ListView listview = (ListView) findViewById(R.id.lstView);
        final String[] values = AnimalList.getNameArray();

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {

                Intent animalActivity = new Intent(MainActivity.this, AnimalActivity.class);
                String name = listview.getItemAtPosition(position).toString();
                animalActivity.putExtra("name", name);

                startActivity(animalActivity);
            }
        });*/

        final RecyclerView recyclerview = (RecyclerView) findViewById(R.id.recyclerView);
        final IconicAdapter adapter = new IconicAdapter(values);

        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerview.setAdapter(adapter);
    }

    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {

        private String[] values;

        public IconicAdapter(String[] values1){

            values = values1;
        }

        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = getLayoutInflater().inflate(R.layout.row, parent, false);
            RowHolder rowHolder = new RowHolder(itemView);
            return (rowHolder);
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position) {

            String name = values[position];
            Animal animal = AnimalList.getAnimal(name);

            int idImg = holder.image.getResources().getIdentifier(animal.getImgFile(), "drawable", getPackageName());

            holder.texte.setText(name);
            holder.image.setImageResource(idImg);
        }

        @Override
        public int getItemCount() {

            return (values.length);
        }
    }

    static class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView texte;
        ImageView image;

        RowHolder(View row){
            super(row);

            texte = (TextView)row.findViewById(R.id.txt);
            image = (ImageView)row.findViewById(R.id.icone);
            row.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            Intent intent = new Intent(view.getContext(), AnimalActivity.class);
            String name = texte.getText().toString();
            intent.putExtra("name", name);
            view.getContext().startActivity(intent);
        }
    }
}

package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        final TextView titre = (TextView) findViewById(R.id.titre);
        final ImageView image = (ImageView) findViewById(R.id.imgView);

        final TextView textview1 = (TextView) findViewById(R.id.txtView1);
        final TextView textview2 = (TextView) findViewById(R.id.txtView2);
        final TextView textview3 = (TextView) findViewById(R.id.txtView3);
        final TextView textview4 = (TextView) findViewById(R.id.txtView4);

        final EditText editText = (EditText) findViewById(R.id.editText);

        final Button button = (Button) findViewById(R.id.button);

        Intent intent = getIntent();
        if (intent != null) {

            if (intent.hasExtra("name")) {
                String name = intent.getStringExtra("name");
                final Animal animal = AnimalList.getAnimal(name);

                int idImg = image.getResources().getIdentifier(animal.getImgFile(), "drawable", getPackageName());
                image.setImageResource(idImg);

                String esperance_de_vie_max = animal.getStrHightestLifespan();
                String periode_de_gestation = animal.getStrGestationPeriod();
                String poids_naissance = animal.getStrBirthWeight();
                String poids_adulte = animal.getStrAdultWeight();
                String statut = animal.getConservationStatus();

                titre.setText(name);
                textview1.setText(esperance_de_vie_max);
                textview2.setText(periode_de_gestation);
                textview3.setText(poids_naissance);
                textview4.setText(poids_adulte);
                editText.setText(statut);

                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        animal.setConservationStatus(editText.getText().toString());
                    }
                });

            }
        }
    }
}
